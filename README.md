# QC#
[![pipeline status](https://gitlab.com/quantum-core/core-dotnet/badges/master/pipeline.svg)](https://gitlab.com/quantum-core/core-dotnet/commits/master)

**QC#** (QuantumCore) is a reinvented open source implementation of the Metin2 server.

It is based on [QuantumCore's C++](https://gitlab.com/quantum-core/core/) version, and is the version which will get continued to be supported.

Metin2, and all Metin2 art, images, and lore are copyrighted by [Webzen](http://webzen.com/ "Webzen").

## Maintainers
This project is currently maintained by:
- NoFr1ends
- Arves100
- pollux

## Authors & contributors
For a full list of all authors and contributors see [AUTHORS.md](AUTHORS.md)

## Community and contributing
To get started contributing to the project, see the [contributing guide](CONTRIBUTING.md).

To get in touch with the developers, the best way is to join our [Discord server](https://discord.gg/6VhbYxX).

## Copyright
License: **Mozilla Public License 2.0**

For more details see [LICENSE](LICENSE)

## Partners & Supporters
QuantumCore is supported by JetBrains which provide us with free licenses for their products.

[<img src="docs/images/jetbrains.png" alt="JetBrains" width=150>](https://www.jetbrains.com/?from=QuantumCore)
