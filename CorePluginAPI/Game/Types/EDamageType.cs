﻿namespace QuantumCore.Game.PlayerUtils
{
    public enum EDamageType
    {
        None,
        Normal,
        NormalRange,
        Melee,
        Range,
        Fire,
        Ice,
        Elec,
        Magic,
        Poison,
        Special
    }
}