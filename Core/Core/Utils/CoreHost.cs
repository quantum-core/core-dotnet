﻿using System.Net;

namespace QuantumCore.Core.Utils;

public struct CoreHost
{
    public IPAddress Ip;
    public ushort Port;
}