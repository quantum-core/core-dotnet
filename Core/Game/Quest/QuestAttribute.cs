using System;
using JetBrains.Annotations;

namespace QuantumCore.Game.Quest;

[MeansImplicitUse]
[AttributeUsage(AttributeTargets.Class, Inherited = false)]
public class QuestAttribute : Attribute
{
    
}